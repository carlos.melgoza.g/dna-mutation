'use strict';

const mod = require('./handler');

const jestPlugin = require('serverless-jest-plugin');
const lambdaWrapper = jestPlugin.lambdaWrapper;
const wrapped = lambdaWrapper.wrap(mod, { handler: 'hasMutationHandler' });

describe('hasMutation', () => {
  it('returns parameters in the body', () => {
    return wrapped.run({queryStringParameters: { a: 'b'}}).then((response) => {
      expect(response.statusCode).toEqual(403);
    });
  });


});
