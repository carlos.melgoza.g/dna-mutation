const mysql = require("mysql");

const rds = {
    host: 'dnamutationn.ckyil4vffxux.us-east-1.rds.amazonaws.com',
    port: '3306',
    user: 'dnamutationn',
    password: 'dnamutationn',
    database: 'mutation'
};

function initConnection(database){
    function disconnect (connection){
        connection.on("err", err =>{
            if (err instanceof Error){
                if(err.code === "PROTOCOL_CONNECTION_LOST"){
                    initConnection(connection.config);
                } else if(err.fatal){
                    throw err;
                }
            }
        } );
    }

    let con = mysql.createConnection(database);

    disconnect(con);
    con.connect();
    return con;
}

const connection = initConnection(rds);

module.exports = connection
