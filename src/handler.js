'use strict';

const mysql = require('serverless-mysql')({
    config:  {
        host: 'twitter.ckyil4vffxux.us-east-1.rds.amazonaws.com',
        user: 'twitter_prod',
        password: '5BXtbSmM8I8Oy^U5Vty0e',
        database: 'twitter'
    }
})

exports.hasMutationHandler = async function(event, context, callback) {
    const response = {
        statusCode: 403,
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        body: ''
    };
    if(event.body){
        const body = JSON.parse(event.body);

        if (body.dna){
            const mutation = hasMutation(body.dna);
            if(mutation){
                response.statusCode = 200;
                response.body = "Has mutation"
            }
            const sql = "UPDATE result SET total = total + 1" + (mutation ? ", has_mutation = has_mutation + 1": "");


            let results = await mysql.query(sql)

        }
    }

    return response;

};

exports.delete = async function(event, context, callback) {
    const response = {
        statusCode: 403,
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        body: ''
    };
    if(event.body){
        console.log(event.body)
        console.log(event.body.id)
        const body = JSON.parse(event.body);

        if (body.id){
            const id = body.id;
            response.statusCode = 200;
            response.body = "Deleted"
            const sql = "DELETE post WHERE id = " + id;
            let results = await mysql.query(sql)

        }
    }

    return response;

};

exports.stats = async function(event, context, callback) {
    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        body: ''
    }
    const sql = 'SELECT * FROM comment_user'
    let results = await mysql.query(sql);
    response.body = JSON.stringify(results)

    return response;

};
exports.config = async function(event, context, callback) {
    const response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Headers" : "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,GET"
        },
        body: ''
    }
    const sql = 'SELECT * FROM config'
    let results = await mysql.query(sql);
    response.body = JSON.stringify(results)

    return response;

};


exports.graficasAspel = async function(event, context, callback) {
    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        body: 'Graficas Aspel'
    }

    return response;

};



function hasMutation(arr) {

    let horizontalMap = [];
    let verticalMap = [];
    let mapDiagonal1 = {};
    let mapDiagonal2 = {};
    let next_diagonal1_x = 0;
    let next_diagonal1_y = 0;
    let next_diagonal2_x = arr.length;
    let next_diagonal2_y = 0;
    let mutations = 0;

    for(let j = 0; j < arr.length; j++){
        let string = arr[j];
        horizontalMap.push({});
        for (let i = 0; i < string.length; i++){
            const currentChar = string[i];
            //horizontal check
            if(!(currentChar in horizontalMap[j])){
                horizontalMap[j][currentChar] = 0;
            }
            horizontalMap[j][currentChar]++;
            if(horizontalMap[j][currentChar] === 4){
                if(mutations > 0){
                    return true;
                }
                else{
                    mutations++;
                }
            }

            //Vertical check
            if(!verticalMap[i]){
                verticalMap.push({});
            }
            if(!(currentChar in verticalMap[i])){
                verticalMap[i][currentChar] = 0;
            }
            verticalMap[i][currentChar] ++;
            if(verticalMap[i][currentChar] === 4){
                if(mutations > 0){
                    return true;
                }
                else{
                    mutations++;
                }
            }


            if(next_diagonal1_x === i && next_diagonal1_y === j){
                next_diagonal1_y++;
                next_diagonal1_x++;
                if(!(currentChar in mapDiagonal1)){
                    mapDiagonal1[currentChar] = 0;
                }
                mapDiagonal1[currentChar] ++;
                if(mapDiagonal1[currentChar] === 4){
                    if(mutations > 0){
                        return true;
                    }
                    else{
                        mutations++;
                    }
                }
            }



            if(next_diagonal2_x === i && next_diagonal2_y === j){
                next_diagonal2_y++;
                next_diagonal2_x--;
                if(!(currentChar in mapDiagonal2)){
                    mapDiagonal2[currentChar] = 0;
                }
                mapDiagonal2[currentChar] ++;
                if(mapDiagonal2[currentChar] === 4){
                    if(mutations > 0){
                        return true;
                    }
                    else{
                        mutations++;
                    }
                }
            }
        }
    }

    return false;

}
