function hasMutation(arr) {

    let horizontalMap = [];
    let verticalMap = [];
    let mapDiagonal1 = {};
    let mapDiagonal2 = {};
    let next_diagonal1_x = 0;
    let next_diagonal1_y = 0;
    let next_diagonal2_x = arr.length;
    let next_diagonal2_y = 0;
    let mutations = 0;

    for(let j = 0; j < arr.length; j++){
        let string = arr[j];
        horizontalMap.push({});
        for (let i = 0; i < string.length; i++){
            const currentChar = string[i];
            //horizontal check
            if(!(currentChar in horizontalMap[j])){
                horizontalMap[j][currentChar] = 0;
            }
            horizontalMap[j][currentChar]++;
            if(horizontalMap[j][currentChar] === 4){
                if(mutations > 0){
                    return true;
                }
                else{
                    mutations++;
                }
            }

            //Vertical check
            if(!verticalMap[i]){
                verticalMap.push({});
            }
            if(!(currentChar in verticalMap[i])){
                verticalMap[i][currentChar] = 0;
            }
            verticalMap[i][currentChar] ++;
            if(verticalMap[i][currentChar] === 4){
                if(mutations > 0){
                    return true;
                }
                else{
                    mutations++;
                }
            }


            if(next_diagonal1_x === i && next_diagonal1_y === j){
                next_diagonal1_y++;
                next_diagonal1_x++;
                if(!(currentChar in mapDiagonal1)){
                    mapDiagonal1[currentChar] = 0;
                }
                mapDiagonal1[currentChar] ++;
                if(mapDiagonal1[currentChar] === 4){
                    if(mutations > 0){
                        return true;
                    }
                    else{
                        mutations++;
                    }
                }
            }



            if(next_diagonal2_x === i && next_diagonal2_y === j){
                next_diagonal2_y++;
                next_diagonal2_x--;
                if(!(currentChar in mapDiagonal2)){
                    mapDiagonal2[currentChar] = 0;
                }
                mapDiagonal2[currentChar] ++;
                if(mapDiagonal2[currentChar] === 4){
                    if(mutations > 0){
                        return true;
                    }
                    else{
                        mutations++;
                    }
                }
            }
        }
    }

    return false;

}
console.log(hasMutation([ "ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]));
console.log(hasMutation(["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA","TCACTG"]));

